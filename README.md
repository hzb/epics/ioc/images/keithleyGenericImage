![pipeline](https://codebase.helmholtz.cloud/hzb/epics/ioc/images/simmotorgenericimage/badges/pipeline_branch/pipeline.svg)

### Generic Sim Motor Dockerfile

This directory contains the repository of the dockerfile for building an IOC for connecting to a simulated EPICs Motor Record

You need to specify the PV Prefix when you use this image

You must use host networking. 

The file RELEASE.local contains the names of each of the support modules and their locations.

### Modifying the IOC Source

The source code for the IOC used in this image is here: https://codebase.helmholtz.cloud/hzb/epics/ioc/simmotor/simmotoriocsource

You can use this image as a development environment. Pull it, create a container from it, attach to that container in a VS Code dev_container and then navigate to $IOC/simMotor

You can make and test changes there. You will need to export the following environment variables which are expected by the IOC. Here are some examples

```
export IOC_DEV=SIMMOTOR
export IOC_SYS=TEST
```

Assuming you have SSH setup in you VS Code host environment and that host has write access to the repo, you should be able to push to the remote. If you do that with a new tag you can then update this Dockerfile and generate a new image. We should be able to do this in a pipeline eventually. 

### Pipeline structure 

    1. It checks whether there is an image with the tag latest during the initia_build stage. If yes then it moves to stage text_latest, where it validates the image and puts the output into the artifacts.
    2. If you merge changes to main it will always run the build process with tag latest and test it. The result of the test will be in test_main.log.
    3. If you create a new tag, an image will be created and the logs will be in the test_tag.log
